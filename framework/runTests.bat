@ECHO OFF
setlocal EnableDelayedExpansion

set bool[0]=-
set bool[1]=+
set /a opt1=1
set /a opt2=1
set /a opt3=1
set /a opt4=1
set /a opt5=1
set /a testCount=0

set hour=%time:~0,2%
if "%hour:~0,1%" == " " set hour=0%hour:~1,1%
set min=%time:~3,2%
if "%min:~0,1%" == " " set min=0%min:~1,1%
set secs=%time:~6,2%
if "%secs:~0,1%" == " " set secs=0%secs:~1,1%
set year=%date:~-4%
set month=%date:~3,2%
set day=%date:~0,2%
if "%day:~0,1%" == " " set day=0%day:~1,1%

set datetime=%year%%month%%day%-%hour%%min%%secs%

:a
cls
echo/
echo Enter keys '1' to '5' to toggle test suites, 'x' to execute tests or 'q' to quit
echo Please select which test suites you would like to run:
echo/
echo [!bool[%opt1%]!] 1) Categories
echo [!bool[%opt2%]!] 2) Nationalities
echo [!bool[%opt3%]!] 3) Comments
echo [!bool[%opt4%]!] 4) Posts
echo [!bool[%opt5%]!] 5) Navigation
echo/
set /P "userInput=Enter your choice: "

if %userInput% == q ( goto q )
if %userInput% == 1 ( if %opt1% == 1 ( set /a opt1=0 ) else ( set /a opt1=1 ) )
if %userInput% == 2 ( if %opt2% == 1 ( set /a opt2=0 ) else ( set /a opt2=1 ) )
if %userInput% == 3 ( if %opt3% == 1 ( set /a opt3=0 ) else ( set /a opt3=1 ) )
if %userInput% == 4 ( if %opt4% == 1 ( set /a opt4=0 ) else ( set /a opt4=1 ) )
if %userInput% == 5 ( if %opt5% == 1 ( set /a opt5=0 ) else ( set /a opt5=1 ) )
if %userInput% == x ( goto init )

goto a

:init
    if %opt1% == 1 ( goto clean )
    if %opt2% == 1 ( goto clean )
    if %opt3% == 1 ( goto clean )
    if %opt4% == 1 ( goto clean )
    if %opt5% == 1 ( goto clean )

    goto cancel;

:clean
    call mvn install -Dmaven.test.skip=true
    call mvn clean

:cond1
    if %opt1% == 1 ( goto test1 )
:cond2
    if %opt2% == 1 ( goto test2 )
:cond3
    if %opt3% == 1 ( goto test3 )
:cond4
    if %opt4% == 1 ( goto test4 )
:cond5
    if %opt5% == 1 ( goto test5 )
:cancel
    if %testCount% == 0 ( echo/ & echo No tests selected to execute ) else ( goto report )
    goto q

:report
    call mvn surefire-report:report-only
    xcopy /S /F /Q /Y target\site\surefire-report.html "reports\Report-%datetime%.html*" >NUL
	
	echo/
	echo "Created the report in this location: /reports"
    goto q

:test1
    call mvn test -Dtest=runner/Categories.java
    set /A testCount=%testCount%+1
    goto cond2
:test2
    call mvn test -Dtest=runner/Nationalities.java
    set /A testCount=%testCount%+1
    goto cond3
:test3
    call mvn test -Dtest=runner/Comments.java
    set /A testCount=%testCount%+1
    goto cond4
:test4
    call mvn test -Dtest=runner/Posts.java
    set /A testCount=%testCount%+1
    goto cond5
:test5
    call mvn test -Dtest=NavigationTests/NavigationTests.java
    set /A testCount=%testCount%+1
    goto report

:q
echo/
echo Script finished executing successfully!
echo/