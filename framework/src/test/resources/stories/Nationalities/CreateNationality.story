Meta:
@endToEnd
@nationalityFunctionality

Narrative:
As an administrator
I want to create a new nationality
So that I can show nationalities functionality

Scenario: Add a new nationality
Given I am logged in as administrator
When I create a new nationality
Then A new nationality is created