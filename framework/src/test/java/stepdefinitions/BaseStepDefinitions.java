package stepdefinitions;

import com.telerikacademy.finalproject.utils.UserActions;
import org.jbehave.core.annotations.AfterStories;
import org.jbehave.core.annotations.BeforeScenario;

public class BaseStepDefinitions {

    @BeforeScenario
    public void beforeScenario(){
        UserActions.loadBrowser();
    }

    @AfterStories
    public void afterStories(){
        UserActions.quitDriver();
    }
}
