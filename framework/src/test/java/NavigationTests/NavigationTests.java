package NavigationTests;

import com.telerikacademy.finalproject.pages.*;
import org.junit.Test;

public class NavigationTests extends BaseTest {

    HomePage homePage = new HomePage();
    LoginPage loginPage = new LoginPage();
    RegisterPage registerPage = new RegisterPage();
    PostsPage postsPage = new PostsPage();
    UsersPage usersPage = new UsersPage();
    ProfilePage profilePage = new ProfilePage();
    CategoryPage categoryPage = new CategoryPage();
    NationalityPage nationalityPage = new NationalityPage();
    NavigationPage navPage = new NavigationPage();
    AboutPage aboutPage = new AboutPage();

    @Test
    public void LoggedOut_NavigateToHomePage(){
        loginPage.logOutAndReturnToHomepage();
        navPage.navigateTo("homepage");
        homePage.assertPageNavigated();
    }

    @Test
    public void LoggedOut_NavigateToSignIn(){
        loginPage.logOutAndReturnToHomepage();
        navPage.navigateTo("login");
        loginPage.assertPageNavigated();
    }

    @Test
    public void LoggedOut_NavigateToSignUp(){
        loginPage.logOutAndReturnToHomepage();
        navPage.navigateTo("register");
        registerPage.assertPageNavigated();
    }

    @Test
    public void LoggedOut_NavigateToLatestPosts(){
        loginPage.logOutAndReturnToHomepage();
        navPage.navigateTo("posts");
        postsPage.assertPageNavigated();
    }

    @Test
    public void AsUser_NavigateToConnectedPosts(){
        loginPage.logInAs("regular user");
        navPage.navigateTo("connected posts");
        postsPage.assertPageNavigated("/connected");
    }

    @Test
    public void AsUser_NavigateToNewPost(){
        loginPage.logInAs("regular user");
        navPage.navigateTo("new post");
        postsPage.assertPageNavigated("/new");
    }

    @Test
    public void AsUser_NavigateToProfile(){
        loginPage.logInAs("regular user");
        navPage.navigateTo("profile");
        profilePage.assertPageNavigated("/");
    }

    @Test
    public void AsUser_NavigateToProfileUsingSubMenu(){
        loginPage.logInAs("regular user");
        navPage.navigateTo("my profile");
        profilePage.assertPageNavigated("/");
    }

    @Test
    public void AsUser_NavigateToUsers(){
        loginPage.logInAs("regular user");
        navPage.navigateTo("users");
        usersPage.assertPageNavigated("users");
    }

    @Test
    public void AsUser_NavigateToRequests(){
        loginPage.logInAs("regular user");
        navPage.navigateTo("requests");
        usersPage.assertPageNavigated("connections/requests");
    }

    @Test
    public void AsUser_NavigateToFriends(){
        loginPage.logInAs("regular user");
        navPage.navigateTo("friends");
        usersPage.assertPageNavigated("connections");
    }

    @Test
    public void AsUser_NavigateToUsersUsingSubMenu(){
        loginPage.logInAs("regular user");
        navPage.navigateTo("all users");
        usersPage.assertPageNavigated("users");
    }

    @Test
    public void AsAdmin_NavigateToCategories(){
        loginPage.logInAs("administrator");
        navPage.navigateTo("categories");
        categoryPage.assertPageNavigated();
    }

    @Test
    public void AsAdmin_NavigateToNationalities(){
        loginPage.logInAs("administrator");
        navPage.navigateTo("nationalities");
        nationalityPage.assertPageNavigated();
    }

    @Test
    public void AsAdmin_NavigateToAboutUs(){
        loginPage.logOutAndReturnToHomepage();
        navPage.navigateTo("about us");
        aboutPage.assertPageNavigated();
    }

}
