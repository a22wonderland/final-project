Meta:
@endToEnd
@adminPostFunctionality

Narrative:
As an administrator
I want to delete my public post
So that my post is deleted

Scenario: Delete a public post as an administrator
Given I am logged in as administrator
When I delete a public post
Then A public post is deleted