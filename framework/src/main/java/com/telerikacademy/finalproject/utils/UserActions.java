package com.telerikacademy.finalproject.utils;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;

import static org.junit.Assert.fail;

public class UserActions {
    public WebDriver getDriver() {
        return driver;
    }

    final WebDriver driver;
    final Actions actions;

    public UserActions() {
        this.driver = Utils.getWebDriver();
        this.actions = new Actions(Utils.getWebDriver());
    }

    public static void loadBrowser() {
        Utils.getWebDriver().get(Utils.getConfigPropertyByKey("base.url") + Utils.getConfigPropertyByKey("logout.slug"));
        Utils.getWebDriver().get(Utils.getConfigPropertyByKey("base.url"));
    }

    public static void quitDriver() {
        Utils.tearDownWebDriver();
    }

    //############# ELEMENT OPERATIONS #########

    public void clickElement(String key, Object... arguments) {
        String locator = Utils.getUIMappingByKey(key, arguments);

        Utils.LOG.info("Clicking on element " + key);
        WebDriverWait wait = new WebDriverWait(Utils.getWebDriver(), 10);
        wait.until(ExpectedConditions.elementToBeClickable((Utils.getWebDriver().findElement(By.xpath(locator)))));
        WebElement element = driver.findElement(By.xpath(locator));
        element.click();
    }

    public void select(String value, String select, Object... arguments) {
        String locator = Utils.getUIMappingByKey(select, arguments);

        Utils.LOG.info("Selecting " + value + " from dropdown " + select);
        Select element = new Select(driver.findElement(By.xpath(locator)));
            element.selectByValue(value);
    }

    public void select(String[] values, String select, Object... arguments) {
        String locator = Utils.getUIMappingByKey(select, arguments);

        Utils.LOG.info("Selecting " + Arrays.toString(values) + " from dropdown " + select);
        Select element = new Select(driver.findElement(By.xpath(locator)));
        for (String value : values) {
            element.selectByValue(value);
        }
    }

    public void typeValueInField(String value, String field, Object... fieldArguments) {
        String locator = Utils.getUIMappingByKey(field, fieldArguments);
        WebElement element = driver.findElement(By.xpath(locator));
        element.sendKeys(value);
    }

    public void hoverMenuThenClickElement(String menu, String element, Object... arguments) {
        String menuLocator = Utils.getUIMappingByKey(menu, arguments);
        String elementLocator = Utils.getUIMappingByKey(element, arguments);

        Utils.LOG.info("Hovering on " + menu + " and clicking " + element);
        WebElement menuElement = driver.findElement(By.xpath(menuLocator));
        WebElement subMenuElement = driver.findElement(By.xpath(elementLocator));
        new WebDriverWait(Utils.getWebDriver(), 10).until(
                webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
        actions.moveToElement(menuElement).pause(200).moveToElement(subMenuElement).pause(200).click().pause(1000).build().perform();

    }


    //############# WAITS #########

    public boolean isElementPresentUntilTimeout(String locator, int seconds, Object... arguments) {
        try {
            String xpath = Utils.getUIMappingByKey(locator, arguments);
            WebDriverWait wait = new WebDriverWait(driver, seconds);
            wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(xpath)));
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    public void waitForElementVisible(String locator, int seconds) {
        WebElement element= driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
        WebDriverWait wait= new WebDriverWait(driver,seconds);
        try {
            wait.until(ExpectedConditions.visibilityOf(element));
        }
        catch (Exception exception){
            fail("Element with locator: '" + locator + "' was not found.");
        }
    }

    //############# ASSERTS #########

    public void assertElementPresent(String locator, Object... arguments) {
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator, arguments))));
    }

    public void assertElementIsNotPresent(String locator, Object... arguments) {
        try {
            driver.findElement(By.xpath(Utils.getUIMappingByKey(locator, arguments)));
            fail("Element " + locator + " is present");
        } catch (NoSuchElementException ex) {
            /* do nothing, element is not present, assert is passed */
        }
    }

}
