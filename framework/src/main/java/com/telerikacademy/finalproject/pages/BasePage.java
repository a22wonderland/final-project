package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.UserActions;
import com.telerikacademy.finalproject.utils.Utils;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import java.sql.Timestamp;

public abstract class BasePage {
    protected String url;
    protected WebDriver driver;
    public UserActions actions;

    protected static final int DEFAULT_TIMEOUT = Integer.parseInt(Utils.getConfigPropertyByKey("config.defaultTimeoutSeconds"));

    public BasePage(String slug) {
        String pageUrl = Utils.getConfigPropertyByKey("base.url") + Utils.getConfigPropertyByKey(slug);
        this.driver = Utils.getWebDriver();
        this.url = pageUrl;
        actions = new UserActions();
    }

    public BasePage() {
        String pageUrl = Utils.getConfigPropertyByKey("base.url");
        this.driver = Utils.getWebDriver();
        this.url = pageUrl;
        actions = new UserActions();
    }

    public String getUrl() {
        return url;
    }

    protected static String getTimestamp() {
        return new Timestamp(System.currentTimeMillis()).toString();
    }

    public void navigateToPage() {
        this.driver.get(url);
    }

    public void navigateToPage(String urlExtension) {
        this.driver.get(url + urlExtension);
    }

    public void assertPageNavigated() {
        String currentUrl = driver.getCurrentUrl();
        Assert.assertTrue("Landed URL is not as expected. Actual URL: " + currentUrl + ". Expected URL: " + url, currentUrl.contains(url));
    }

    public void assertPageNavigated(String urlExtension) {
        String currentUrl = driver.getCurrentUrl();
        Assert.assertTrue("Landed URL is not as expected. Actual URL: " + currentUrl + ". Expected URL: " + url + urlExtension, currentUrl.contains(url + urlExtension));
    }
}
