Meta:
@endToEnd
@userPostFunctionality

Narrative:
As a regular user
I want to create a new public post
So that a public post is created

Scenario: Create a new public post as a regular user
Given I am logged in as regular user
When I create a new public post
Then A new public post is created