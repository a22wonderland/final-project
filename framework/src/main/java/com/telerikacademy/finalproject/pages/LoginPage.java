package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.UserActions;
import com.telerikacademy.finalproject.utils.Utils;

public class LoginPage extends BasePage {

    public LoginPage() {
        super("login.slug");
    }

    public final String usernameField = "login.Username";
    public final String passwordField = "login.Password";
    public final String loginButton = "login.LoginButton";

    public final String adminUsername = Utils.getConfigPropertyByKey("healthyFoodAdmin.username");
    public final String adminPassword = Utils.getConfigPropertyByKey("healthyFoodAdmin.password");

    public final String userUsername = Utils.getConfigPropertyByKey("regularUser.username");
    public final String userPassword = Utils.getConfigPropertyByKey("regularUser.password");


    public void logOutAndReturnToHomepage() {
        UserActions.loadBrowser();
    }

    public void logInAs(String user) {
        navigateToPage();
        switch (user) {
            case "administrator":
                actions.typeValueInField(adminUsername, usernameField);
                actions.typeValueInField(adminPassword, passwordField);
                break;
            case "regular user":
                actions.typeValueInField(userUsername, usernameField);
                actions.typeValueInField(userPassword, passwordField);
                break;
            default:
                throw new IllegalStateException("User not found: " + user);
        }
        actions.clickElement(loginButton);
    }

}
