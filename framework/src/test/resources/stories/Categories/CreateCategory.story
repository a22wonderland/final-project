Meta:
@endToEnd
@categoryFunctionality

Narrative:
As an administrator
I want to create a new category
So that I can show categories functionality

Scenario: Add a new category
Given I am logged in as administrator
When I create a new category
Then A new category is created
