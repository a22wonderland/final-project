package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Utils;

public class NationalityPage extends BasePage{
    public NationalityPage(){
        super("nationalities.slug");
    }

    private static final String NEW_NATIONALITY_TITLE = "test " + getTimestamp();

    public final String addNationality = "nationalities.Add";
    public final String nationalityName = "nationalities.new.Name";
    public final String nationalitySaveButton = "nationalities.new.SaveButton";
    public final String nationalityTitle = "nationalities.Title";
    public final String editNationality = "nationalities.EditButton";
    public final String editNationalityDeleteButton = "nationalities.edit.DeleteButton";
    public final String nationalitySubmitButton = "nationalities.edit.SubmitButton";

    public final String submitButton = "generic.SubmitButton";

    public void createNewNationality() {
        navigateToPage();
        if (actions.isElementPresentUntilTimeout(addNationality, DEFAULT_TIMEOUT)){
            actions.clickElement(addNationality);
            actions.typeValueInField(NEW_NATIONALITY_TITLE, nationalityName);
            actions.clickElement(submitButton);
        }
    }

    public void assertNewNationalityIsCreated() {
        navigateToPage();
        if (actions.isElementPresentUntilTimeout(editNationality, DEFAULT_TIMEOUT, NEW_NATIONALITY_TITLE)) {
            actions.assertElementPresent(nationalityTitle, NEW_NATIONALITY_TITLE);
        }
    }

    public void deleteNationality() {
        navigateToPage();
        if (actions.isElementPresentUntilTimeout(editNationality, DEFAULT_TIMEOUT, NEW_NATIONALITY_TITLE)) {
            actions.clickElement(editNationality, NEW_NATIONALITY_TITLE);
            actions.clickElement(editNationalityDeleteButton);
            actions.clickElement(submitButton);
        }
    }

    public void assertNationalityIsDeleted() {
        navigateToPage();
        if (actions.isElementPresentUntilTimeout(addNationality, DEFAULT_TIMEOUT)) {
            actions.assertElementIsNotPresent(nationalityTitle, NEW_NATIONALITY_TITLE);
        }
    }
}
