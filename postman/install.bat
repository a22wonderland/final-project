@ECHO OFF

echo Installing dependencies...
call npm install -g newman >> output.log
call npm install -g newman-reporter-htmlextra >> output.log
call npm install -g execsql >> output.log