Meta:
@endToEnd
@commentFunctionality

Narrative:
As an administrator
I want to delete a post comment
So that I can show comment functionality

Scenario: Delete a post comment
Given A comment is posted
When I delete a post comment
Then The comment is deleted