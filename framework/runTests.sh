#!/bin/bash
cd "`dirname "$0"`"

bool=('-' '+')
date=$(date +%Y%m%d-%H%M%S)

opt1=1
opt2=1
opt3=1
opt4=1
opt5=1

while true
do
	clear
	echo 
	echo "Enter keys '1' to '5' to toggle test suites, 'x' to execute tests or 'q' to quit"
	echo "Please select which test suites you would like to run:"
	echo
	echo "[${bool[opt1]}] 1) Categories"
	echo "[${bool[opt2]}] 2) Nationalities"
	echo "[${bool[opt3]}] 3) Comments"
	echo "[${bool[opt4]}] 4) Posts"
	echo "[${bool[opt5]}] 5) Navigation"
	echo

	read -p "Enter your choice: " userInput
    
	if [[ $userInput == 'q' ]]; then
		break
	elif [[ $userInput == '1' ]]; then
		if [[ $opt1 == 1 ]]; then opt1=0; else opt1=1; fi;
	elif [[ $userInput == '2' ]]; then
		if [[ $opt2 == 1 ]]; then opt2=0; else opt2=1; fi;
	elif [[ $userInput == '3' ]]; then
		if [[ $opt3 == 1 ]]; then opt3=0; else opt3=1; fi;
	elif [[ $userInput == '4' ]]; then
		if [[ $opt4 == 1 ]]; then opt4=0; else opt4=1; fi;
	elif [[ $userInput == '5' ]]; then
		if [[ $opt5 == 1 ]]; then opt5=0; else opt5=1; fi;
	elif [[ $userInput == 'x' ]]; then
		if [[ $opt1 == 0 && $opt2 == 0 && $opt3 == 0 && $opt4 == 0 && $opt5 == 0 ]]; then
			echo 
			echo No tests selected to execute
			break
		fi
		mvn install -Dmaven.test.skip=true
		mvn clean
		if [[ $opt1 == 1 ]]; then mvn test -Dtest=runner/Categories.java; fi;
		if [[ $opt2 == 1 ]]; then mvn test -Dtest=runner/Nationalities.java; fi;
		if [[ $opt3 == 1 ]]; then mvn test -Dtest=runner/Comments.java; fi;
		if [[ $opt4 == 1 ]]; then mvn test -Dtest=runner/Posts.java; fi;
		if [[ $opt5 == 1 ]]; then mvn test -Dtest=NavigationTests/NavigationTests.java; fi;
		mvn surefire-report:report-only
		[ -d reports ] || mkdir reports
		cp target/site/surefire-report.html "reports/Report-$date.html"
		echo
		echo "Created the report in this location: /reports"
		break
	fi
done
echo
echo Script finished executing successfully!
echo