Meta:
@endToEnd
@nationalityFunctionality

Narrative:
As an administrator
I want to delete a created nationality
So that I can show nationalities functionality

Scenario: Delete a new nationality
Given I am logged in as administrator
When I delete a created nationality
Then The nationality is deleted