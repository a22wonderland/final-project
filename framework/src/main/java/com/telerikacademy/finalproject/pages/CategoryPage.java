package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Utils;

import static org.junit.Assert.fail;

public class CategoryPage extends BasePage{
    public CategoryPage() {
        super("categories.slug");
    }

    private static final String NEW_CATEGORY_TITLE = "Test Title " + getTimestamp();

    public final String addCategory = "categories.Add";
    public final String categoryEmoticon = "categories.new.FileInput";
    public final String categoryName = "categories.new.TitleField";
    public final String categorySaveButton = "categories.new.SaveButton";
    public final String categoryTitle = "categories.Title";
    public final String editCategory = "categories.EditButton";
    public final String editCategoryDeleteButton = "categories.edit.DeleteButton";
    public final String editCategorySubmitButton = "categories.edit.SubmitButton";

    public final String submitButton = "generic.SubmitButton";

    public void createNewCategory() {
        navigateToPage();
        if (actions.isElementPresentUntilTimeout(addCategory, DEFAULT_TIMEOUT)) {
            actions.clickElement(addCategory);
            actions.typeValueInField(Utils.getImagePath(), categoryEmoticon);
            actions.typeValueInField(NEW_CATEGORY_TITLE, categoryName);
            actions.clickElement(submitButton);
        }
    }

    public void assertNewCategoryIsCreated() {
        navigateToPage();
        if (actions.isElementPresentUntilTimeout(categoryTitle, DEFAULT_TIMEOUT, NEW_CATEGORY_TITLE)) {
            actions.assertElementPresent(categoryTitle, NEW_CATEGORY_TITLE);
        }
    }

    public void deleteCategory() {
        navigateToPage();
        if (actions.isElementPresentUntilTimeout(editCategory, DEFAULT_TIMEOUT, NEW_CATEGORY_TITLE)) {
            actions.clickElement(editCategory, NEW_CATEGORY_TITLE);
            actions.clickElement(editCategoryDeleteButton);
            actions.clickElement(submitButton);
        } else{
            fail("Element with locator: '" + editCategory + "' was not found.");
        }
    }

    public void assertCategoryIsDeleted() {
        navigateToPage();
        if (actions.isElementPresentUntilTimeout(addCategory, DEFAULT_TIMEOUT)) {
            actions.assertElementIsNotPresent(categoryTitle, NEW_CATEGORY_TITLE);
        }
    }
}