Meta:
@endToEnd
@userPostFunctionality

Narrative:
As a regular user
I want to delete my public post
So that my post is deleted

Scenario: Delete a public post as a regular user
Given I am logged in as regular user
When I delete a public post
Then A public post is deleted