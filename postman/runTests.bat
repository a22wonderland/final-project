@ECHO OFF
setlocal EnableDelayedExpansion

FOR /F "tokens=1,2 delims==" %%A IN (db.config) DO (set %%A=%%B)

echo Configuring database connection...
call execsql -c "%host%" "%username%" "%password%" >> output.log
echo Executing database script...
call execsql -f "data.sql" >> output.log
echo Running API tests...

set bool[0]=-
set bool[1]=+
set /a opt1=1
set /a opt2=1
set /a opt3=1
set /a opt4=1
set /a opt5=1
set /a opt6=1

:a
cls
echo/
echo Enter keys '1' to '6' to toggle test suites, 'x' to execute tests or 'q' to quit
echo Please select which test suites you would like to run:
echo/
echo [!bool[%opt1%]!] 1) Categories
echo [!bool[%opt2%]!] 2) Comments
echo [!bool[%opt3%]!] 3) Connections
echo [!bool[%opt4%]!] 4) Nationalities
echo [!bool[%opt5%]!] 5) Posts
echo [!bool[%opt6%]!] 6) Users
echo/
set /P "userInput=Enter your choice: "

if %userInput% == q ( goto q )
if %userInput% == 1 ( if %opt1% == 1 ( set /a opt1=0 ) else ( set /a opt1=1 ) )
if %userInput% == 2 ( if %opt2% == 1 ( set /a opt2=0 ) else ( set /a opt2=1 ) )
if %userInput% == 3 ( if %opt3% == 1 ( set /a opt3=0 ) else ( set /a opt3=1 ) )
if %userInput% == 4 ( if %opt4% == 1 ( set /a opt4=0 ) else ( set /a opt4=1 ) )
if %userInput% == 5 ( if %opt5% == 1 ( set /a opt5=0 ) else ( set /a opt5=1 ) )
if %userInput% == 6 ( if %opt6% == 1 ( set /a opt6=0 ) else ( set /a opt6=1 ) )
if %userInput% == x ( goto init )

goto a

:init
if %opt1% == 1 ( goto test )
if %opt2% == 1 ( goto test )
if %opt3% == 1 ( goto test )
if %opt4% == 1 ( goto test )
if %opt5% == 1 ( goto test )
if %opt6% == 1 ( goto test )

echo/
echo No tests selected to execute
goto q

:test


if %opt1% == 1 ( set arg1=--folder Categories ) else ( set arg1= )
if %opt2% == 1 ( set arg2=--folder Comments ) else ( set arg2= )
if %opt3% == 1 ( set arg3=--folder Connections ) else ( set arg3= )
if %opt4% == 1 ( set arg4=--folder Nationalities ) else ( set arg4= )
if %opt5% == 1 ( set arg5=--folder Posts ) else ( set arg5= )
if %opt6% == 1 ( set arg6=--folder Users ) else ( set arg6= )

call newman run files\HealthyFood.postman_collection.json -e environment.json -r htmlextra %arg1% %arg2% %arg3% %arg4% %arg5% %arg6%

:q
echo/
call execsql -f "cleanup.sql" >> output.log
echo Cleaning up posts...
echo/
echo Script finished executing successfully!
echo/