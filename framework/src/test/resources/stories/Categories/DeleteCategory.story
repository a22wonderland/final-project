Meta:
@endToEnd
@categoryFunctionality

Narrative:
As an administrator
I want to delete a created category
So that I can show categories functionality

Scenario: Delete a new category
Given I am logged in as administrator
When I delete a created category
Then The category is deleted
