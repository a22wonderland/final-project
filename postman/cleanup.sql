-- #######################
-- Change to the name of the dababase
-- e.g. "use my-db-name;"

use testdb;

-- #######################

SET FOREIGN_KEY_CHECKS=0;

DELETE FROM `comments` WHERE `post_id` = '1000';
DELETE FROM `posts` WHERE `creator_id` = '1000';
DELETE FROM `posts` WHERE `creator_id` = '1001';

SET FOREIGN_KEY_CHECKS=1;