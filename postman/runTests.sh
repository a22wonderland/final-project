#!/bin/bash
cd "`dirname "$0"`"

filename=db.config
host=($(grep "host" $filename))
username=($(grep "username" $filename))
password=($(grep "password" $filename))

echo Configuring database connection...
execsql -c "${host##*=}" "${username##*=}" "${password##*=}" >> output.log
echo Executing database script...
execsql -f "data.sql" >> output.log
echo Running API tests...

bool=('-' '+')

opt1=1
opt2=1
opt3=1
opt4=1
opt5=1
opt6=1

while true
do
	clear
	echo 
	echo "Enter keys '1' to '6' to toggle collections, 'x' to execute tests or 'q' to quit"
	echo "Please select which test suites you would like to run:"
	echo
	echo "[${bool[opt1]}] 1) Categories"
	echo "[${bool[opt2]}] 2) Comments"
	echo "[${bool[opt3]}] 3) Connections"
	echo "[${bool[opt4]}] 4) Nationalities"
	echo "[${bool[opt5]}] 5) Posts"
	echo "[${bool[opt6]}] 6) Users"
	echo

	read -p "Enter your choice: " userInput
    
	if [[ $userInput == 'q' ]]; then
		break
	elif [[ $userInput == '1' ]]; then
		if [[ $opt1 == 1 ]]; then opt1=0; else opt1=1; fi;
	elif [[ $userInput == '2' ]]; then
		if [[ $opt2 == 1 ]]; then opt2=0; else opt2=1; fi;
	elif [[ $userInput == '3' ]]; then
		if [[ $opt3 == 1 ]]; then opt3=0; else opt3=1; fi;
	elif [[ $userInput == '4' ]]; then
		if [[ $opt4 == 1 ]]; then opt4=0; else opt4=1; fi;
	elif [[ $userInput == '5' ]]; then
		if [[ $opt5 == 1 ]]; then opt5=0; else opt5=1; fi;
	elif [[ $userInput == '6' ]]; then
		if [[ $opt6 == 1 ]]; then opt6=0; else opt6=1; fi;
	elif [[ $userInput == 'x' ]]; then
		if [[ $opt1 == 0 && $opt2 == 0 && $opt3 == 0 && $opt4 == 0 && $opt5 == 0 && $opt6 == 0 ]]; then
			echo 
			echo No tests selected to execute
			break
		fi

        [[ $opt1 = 1 ]] && arg1="--folder Categories" || arg1=""
        [[ $opt2 = 1 ]] && arg2="--folder Comments" || arg2=""
        [[ $opt3 = 1 ]] && arg3="--folder Connections" || arg3=""
        [[ $opt4 = 1 ]] && arg4="--folder Nationalities" || arg4=""
        [[ $opt5 = 1 ]] && arg5="--folder Posts" || arg5=""
        [[ $opt6 = 1 ]] && arg6="--folder Users" || arg6=""
		
        newman run files/HealthyFood.postman_collection.json -e environment.json -r htmlextra $arg1 $arg2 $arg3 $arg4 $arg5 $arg6

		break
	fi
done

echo
echo Cleaning up posts...
execsql -f "cleanup.sql" >> output.log
echo
echo Script finished executing successfully!
echo