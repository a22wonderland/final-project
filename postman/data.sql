-- #######################
-- Change to the name of the dababase
-- e.g. "use my-db-name;"

use testdb;

-- #######################

SET FOREIGN_KEY_CHECKS=0;

DELETE FROM `authorities` WHERE `username` = 'test@user.admin';
DELETE FROM `users_details` WHERE `email` = 'test@user.admin';
DELETE FROM `users` WHERE `username` = 'test@user.admin';
DELETE FROM `authorities` WHERE `username` = 'test@user.one';
DELETE FROM `users_details` WHERE `email` = 'test@user.one';
DELETE FROM `users` WHERE `username` = 'test@user.one';

DELETE FROM `authorities` WHERE `username` = 'test@user.two';
DELETE FROM `users_details` WHERE `email` = 'test@user.two';
DELETE FROM `users` WHERE `username` = 'test@user.two';

DELETE FROM `authorities` WHERE `username` = 'test@delete.me';
DELETE FROM `users_details` WHERE `email` = 'test@delete.me';
DELETE FROM `users` WHERE `username` = 'test@delete.me';

DELETE FROM `connections` WHERE `sender_id` = '1000';

DELETE FROM `comments` WHERE `post_id` = '1000';
DELETE FROM `posts` WHERE `creator_id` = '1000';
DELETE FROM `posts` WHERE `creator_id` = '1001';

DELETE FROM `media` WHERE `media_id` = '1000';

DELETE FROM `categories` WHERE `name` = 'New Category';
DELETE FROM `categories` WHERE `name` = 'Updated Category';

INSERT INTO `users` (`username`, `password`, `enabled`) VALUES ('test@user.admin', '$2y$12$M0XUo6iTsXSwlv2xXKoox.Gt424e2mh6pUZ2zTv4AwNBC3YuBSIHu', '1');
INSERT INTO `users_details` (`user_details_id`, `email`, `first_name`, `last_name`, `picture_id`, `description`, `age`, `gender_id`, `nationality_id`, `enabled`) VALUES ('1000', 'test@user.admin', 'User', 'Admin', '1000', '', '24', '1', '29', '1');
INSERT INTO `authorities` (`username`, `authority`) VALUES ('test@user.admin', 'ROLE_USER');
INSERT INTO `authorities` (`username`, `authority`) VALUES ('test@user.admin', 'ROLE_ADMIN');

INSERT INTO `users` (`username`, `password`, `enabled`) VALUES ('test@user.one', '$2y$12$M0XUo6iTsXSwlv2xXKoox.Gt424e2mh6pUZ2zTv4AwNBC3YuBSIHu', '1');
INSERT INTO `users_details` (`user_details_id`, `email`, `first_name`, `last_name`, `picture_id`, `description`, `age`, `gender_id`, `nationality_id`, `enabled`) VALUES ('1001', 'test@user.one', 'User', 'One', '1000', '', '24', '1', '29', '1');
INSERT INTO `authorities` (`username`, `authority`) VALUES ('test@user.one', 'ROLE_USER');

INSERT INTO `users` (`username`, `password`, `enabled`) VALUES ('test@user.two', '$2y$12$M0XUo6iTsXSwlv2xXKoox.Gt424e2mh6pUZ2zTv4AwNBC3YuBSIHu', '1');
INSERT INTO `users_details` (`user_details_id`, `email`, `first_name`, `last_name`, `picture_id`, `description`, `age`, `gender_id`, `nationality_id`, `enabled`) VALUES ('1002', 'test@user.two', 'User', 'Two', '1000', '', '24', '1', '29', '1');
INSERT INTO `authorities` (`username`, `authority`) VALUES ('test@user.two', 'ROLE_USER');

INSERT INTO `users` (`username`, `password`, `enabled`) VALUES ('test@delete.me', '$2y$12$M0XUo6iTsXSwlv2xXKoox.Gt424e2mh6pUZ2zTv4AwNBC3YuBSIHu', '1');
INSERT INTO `users_details` (`user_details_id`, `email`, `first_name`, `last_name`, `picture_id`, `description`, `age`, `gender_id`, `nationality_id`, `enabled`) VALUES ('1003', 'test@delete.me', 'Delete', 'Me', '1000', '', '24', '1', '29', '1');
INSERT INTO `authorities` (`username`, `authority`) VALUES ('test@delete.me', 'ROLE_USER');

INSERT INTO `connections` (`connection_id`, `sender_id`, `receiver_id`, `status_id`) VALUES (NULL, '1000', '1001', '1');

INSERT INTO `posts` (`post_id`, `visibility_id`, `creator_id`, `title`, `description`, `media_id`, `timestamp`, `enabled`) VALUES (NULL, '2', '1001', 'Test connection post admin', '', '1000', '2022-11-11 11:11:03', '1');
INSERT INTO `posts` (`post_id`, `visibility_id`, `creator_id`, `title`, `description`, `media_id`, `timestamp`, `enabled`) VALUES (NULL, '2', '1000', 'Test connection post user', '', '1000', '2022-11-11 11:11:04', '1');
INSERT INTO `posts` (`post_id`, `visibility_id`, `creator_id`, `title`, `description`, `media_id`, `timestamp`, `enabled`) VALUES (NULL, '1', '1001', 'Test public post 1 admin', '', '1000', '2022-11-11 11:11:05', '1');
INSERT INTO `posts` (`post_id`, `visibility_id`, `creator_id`, `title`, `description`, `media_id`, `timestamp`, `enabled`) VALUES (NULL, '1', '1000', 'Test public post 1 user', '', '1000', '2022-11-11 11:11:06', '1');
INSERT INTO `posts` (`post_id`, `visibility_id`, `creator_id`, `title`, `description`, `media_id`, `timestamp`, `enabled`) VALUES (NULL, '1', '1001', 'Test public post 2 admin', '', '1000', '2022-11-11 11:11:07', '1');
INSERT INTO `posts` (`post_id`, `visibility_id`, `creator_id`, `title`, `description`, `media_id`, `timestamp`, `enabled`) VALUES (NULL, '1', '1000', 'Test public post 2 user', '', '1000', '2022-11-11 11:11:08', '1');
INSERT INTO `posts` (`post_id`, `visibility_id`, `creator_id`, `title`, `description`, `media_id`, `timestamp`, `enabled`) VALUES ('1000', '1', '1000', 'Test public post for comments', '', '1000', '2022-11-11 11:11:09', '1');

INSERT INTO `comments` (`comment_id`, `post_id`, `creator_id`, `description`, `timestamp`, `enabled`) VALUES (NULL, '1000', '1000', 'First Comment', '2022-11-11 11:11:10', '1');
INSERT INTO `comments` (`comment_id`, `post_id`, `creator_id`, `description`, `timestamp`, `enabled`) VALUES (NULL, '1000', '1000', 'Latest Comment', '2022-11-11 11:11:11', '1');
INSERT INTO `comments` (`comment_id`, `post_id`, `creator_id`, `description`, `timestamp`, `enabled`) VALUES (NULL, '1000', '1000', 'Latest Comment', '2022-11-11 11:11:12', '1');
INSERT INTO `comments` (`comment_id`, `post_id`, `creator_id`, `description`, `timestamp`, `enabled`) VALUES (NULL, '1000', '1000', 'Latest Comment', '2022-11-11 11:11:13', '1');
INSERT INTO `comments` (`comment_id`, `post_id`, `creator_id`, `description`, `timestamp`, `enabled`) VALUES (NULL, '1000', '1000', 'Latest Comment', '2022-11-11 11:11:14', '1');
INSERT INTO `comments` (`comment_id`, `post_id`, `creator_id`, `description`, `timestamp`, `enabled`) VALUES (NULL, '1000', '1000', 'Latest Comment', '2022-11-11 11:11:15', '1');

INSERT INTO `media` (`media_id`, `type_id`, `visibility_id`, `path`, `enabled`) VALUES ('1000', '1', '1', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAADCAIAAADZSiLoAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAXSURBVBhXYwAC99sgksF5uTuCAwIMDABABAN2bgJKbQAAAABJRU5ErkJggg==', '1');

SET FOREIGN_KEY_CHECKS=1;