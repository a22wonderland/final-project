Meta:
@endToEnd
@adminPostFunctionality

Narrative:
As an administrator
I want to create a new public post
So that a public post is created

Scenario: Create a new public post as an administrator
Given I am logged in as administrator
When I create a new public post
Then A new public post is created