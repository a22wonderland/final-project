# Testing Healthy Food Social Network

The main aim of the project is to verify that the healthy food social network meets the user requirements and is production ready and insuring stability.

## Control procedures:

 - [Final Project Requirements *(Java)*](https://telerikacademy-my.sharepoint.com/:w:/p/nina_krumova_a22_learn/Eeu8gul6AuJAnK6IQ5BWRpUBHNi2ynRHxVw95bjpiMiDYw?e=q62x8N)
 - [Final Project Assignment *(QA)*](https://telerikacademy-my.sharepoint.com/:b:/p/nina_krumova_a22_learn/EYczSGarESRCit4h32P33I4BxOrJqG3tWjljcON8zN8_yQ?e=PJISnK)
 
## Deliverables:

### 1. Environments:
 - [Wonderland Team Test Environment](https://sleepy-reef-44429.herokuapp.com)
 - [Backup Environment](https://powerful-escarpment-97158.herokuapp.com)

### 2. [Test Plan](https://telerikacademy-my.sharepoint.com/:w:/p/nina_krumova_a22_learn/EZOObjSZGqVGtmR5zBg8YAQBerZJ2IlbxsH1mRf1Vg2w_g?e=AYcwPQ) 
The purpose of the document is to outline the test strategies, the tools and techniques used for the testing process.

### 3. [Exploratory testing notes](https://telerikacademy-my.sharepoint.com/:w:/p/nina_krumova_a22_learn/ESDkb0xVBbtJolAUKC4uZvYBT17a1Y7bRSEIT9WWQQW2Hg?e=YVMsrd),
which are compiled on the basis of the [initial acquaintance](https://telerikacademy-my.sharepoint.com/personal/teodor_nikolov_a22_learn_telerikacademy_com/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Fteodor%5Fnikolov%5Fa22%5Flearn%5Ftelerikacademy%5Fcom%2FDocuments%2Fhealthy%2Dfood%2Dmnd%2Dmap%2Epdf&parent=%2Fpersonal%2Fteodor%5Fnikolov%5Fa22%5Flearn%5Ftelerikacademy%5Fcom%2FDocuments&originalPath=aHR0cHM6Ly90ZWxlcmlrYWNhZGVteS1teS5zaGFyZXBvaW50LmNvbS86YjovcC90ZW9kb3Jfbmlrb2xvdl9hMjJfbGVhcm4vRVpQOTVnN0VJSWRNajNVU01QUXh0VklCMlhITlR1VEVIaEh6Uno4S2czbHVIZz9ydGltZT04N01HSnh1VzJFZw) with the social network and determine some bugs and specific proposals for improving its usage and the user experience.

### 4. [Test Cases](https://telerikacademy-my.sharepoint.com/:x:/p/teodor_nikolov_a22_learn/Eed94eZZty9CrvPb0p46fCABYCWCfFYC12UypKSkDOGF2w?e=cydLEi)
The development of the Test Cases was done with excel, which allows easily to add additional data and gives you the opportunity to do different reports based on different criteria.
Also the whole process of performing the manual testing is carried out on the basis of the test cases described in the document. 
The result of the manual test are presented in a summary report.

### 5. Bugs logging 

[JIRA](https://wonderlandteam.atlassian.net/jira/software/c/projects/FINAL/issues) tool was used for logging bugs.

Jira username: a22wonderlandteam@gmail.com  
Jira password: @NT1234567

### 6. Test Scripts

Test script defines the various steps as well as instructions needed to test the software:
 - [POSTMAN](https://gitlab.com/a22wonderland/final-project/-/tree/master/postman)
 - [Java/Selenium/JBehave](https://gitlab.com/a22wonderland/final-project/-/tree/master/framework)

### 7. Test Report

This documents contains:

- Test Execution Report, which provides results and a summary of the test execution activities: 
	- [POSTMAN Execution Report](https://gitlab.com/a22wonderland/final-project/-/tree/master/reports/api-tests-report.html) 
	- [Selenium Execution Report](https://gitlab.com/a22wonderland/final-project/-/tree/master/reports/ui-tests-report.html) 
- [Test Summary Report](https://gitlab.com/a22wonderland/final-project/-/tree/master/reports/test-summary-report.pdf), which provides summary of the entire testing process. 
 
### 8. Install/config guides
 
 Below you will find install/config guides for each of the automated tools:

- [POSTMAN](https://gitlab.com/a22wonderland/final-project/-/blob/master/postman/README.md)
- [SELENIUM](https://gitlab.com/a22wonderland/final-project/-/blob/master/framework/README.md)