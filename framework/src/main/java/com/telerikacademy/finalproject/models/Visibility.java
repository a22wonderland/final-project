package com.telerikacademy.finalproject.models;

public enum Visibility {
    PUBLIC {
        @Override
        public String toString() {
            return "1";
        }
    },
    CONNECTIONS {
        @Override
        public String toString() {
            return "2";
        }
    },
}
