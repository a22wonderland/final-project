package com.telerikacademy.finalproject.pages;

public class ProfilePage extends BasePage {
    public ProfilePage() {
        super("users.slug");
    }

    @Override
    public void navigateToPage() {
        super.navigateToPage("/profile");
    }

}

