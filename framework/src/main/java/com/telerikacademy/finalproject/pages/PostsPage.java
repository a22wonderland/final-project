package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.models.Visibility;
import com.telerikacademy.finalproject.utils.Utils;

public class PostsPage extends BasePage {

    public PostsPage() {
        super("posts.slug");
    }

    private static final String NEW_POST_TITLE = "Test Title " + getTimestamp();
    private static final String NEW_POST_DESCRIPTION = "Test Description " + getTimestamp();
    private static final String NEW_COMMENT_DESCRIPTION = "Test comment " + getTimestamp();
    private static final String POST_FOR_COMMENT_TITLE = "Test Post " + getTimestamp();

    public final String feed = "posts.Feed";
    public final String searchField = "posts.SearchField";
    public final String searchButton = "posts.SearchButton";
    public final String postTitle = "posts.Title";

    public final String singlePostTitle = "posts.single.Title";
    public final String singlePostDescription = "posts.single.Description";
    public final String singlePostEditButton = "posts.single.Edit";
    public final String deletePostButton = "posts.edit.Delete";
    public final String titleField = "posts.new.Title";
    public final String descriptionField = "posts.new.Description";
    public final String visibilityDropdown = "posts.new.Visibility";
    public final String photoOrVideoInput = "posts.new.PhotoOrVideo";
    public final String categoriesSelect = "posts.new.Categories";
    public final String postSaveButton = "posts.new.Save";

    public final String commentTitle = "comment.Title";
    public final String commentSaveButton = "comment.new.Save";
    public final String leaveCommentField = "post.new.Comment";
    public final String editCommentButton = "comment.Edit";
    public final String deleteCommentButton = "comment.Delete";
    public final String commentSubmitButton = "comment.SubmitButton";

    public final String submitButton = "generic.SubmitButton";

    public void navigateToNewPost() {
        this.driver.get(url + "/new");
    }

    public void navigateToConnectionsPosts() {
        this.driver.get(url + "/connected");
    }

    public void navigateToNewlyCreatedPost() {
        navigateToPage();
        actions.typeValueInField(POST_FOR_COMMENT_TITLE, searchField);
        actions.clickElement(searchButton);
        if(actions.isElementPresentUntilTimeout(postTitle, DEFAULT_TIMEOUT, POST_FOR_COMMENT_TITLE)){
            actions.clickElement(postTitle, POST_FOR_COMMENT_TITLE);
        }
    }

    public void createNewPost(String type) {

        String visibility;

        navigateToNewPost();

        switch (type) {
            case "public":
                actions.typeValueInField(NEW_POST_TITLE, titleField);
                visibility = String.valueOf(Visibility.PUBLIC);
                break;
            case "connections":
                actions.typeValueInField(NEW_POST_TITLE, titleField);
                visibility = String.valueOf(Visibility.CONNECTIONS);
                break;
            case "for comments":
                actions.typeValueInField(POST_FOR_COMMENT_TITLE, titleField);
                visibility = String.valueOf(Visibility.PUBLIC);
                break;
            default:
                throw new IllegalStateException("Unexpected post type: " + type);
        }

        actions.typeValueInField(NEW_POST_DESCRIPTION, descriptionField);
        actions.typeValueInField(Utils.getImagePath(), photoOrVideoInput);
        actions.select(visibility, visibilityDropdown);
        actions.clickElement(postSaveButton);
    }

    public void assertPostCreated(String type) {
        switch (type) {
            case "public":
                navigateToPage();
                actions.typeValueInField(NEW_POST_TITLE, searchField);
                actions.clickElement(searchButton);
                if(actions.isElementPresentUntilTimeout(postTitle,DEFAULT_TIMEOUT, NEW_POST_TITLE)){
                    actions.assertElementPresent(postTitle, NEW_POST_TITLE);
                    actions.clickElement(postTitle, NEW_POST_TITLE);
                    actions.assertElementPresent(singlePostTitle, NEW_POST_TITLE);
                    actions.assertElementPresent(singlePostDescription, NEW_POST_DESCRIPTION);
                }
            case "connections":
                //logic
                break;
            default:
                throw new IllegalStateException("Unexpected post type: " + type);
        }
    }

    public void deletePost(String type) {
        navigateToPage();
        switch (type) {
            case "public":
                if (actions.isElementPresentUntilTimeout(postTitle, DEFAULT_TIMEOUT, NEW_POST_TITLE)) {
                    actions.clickElement(postTitle, NEW_POST_TITLE);
                    actions.clickElement(singlePostEditButton);
                    actions.clickElement(deletePostButton);
                    actions.clickElement(submitButton);
                }
            case "connections":
                //logic
                break;
            default:
                throw new IllegalStateException("Unexpected post type: " + type);
        }
    }

    public void assertPostDeleted(String type) {
        navigateToPage();
        switch (type) {
            case "public":
                if (actions.isElementPresentUntilTimeout(feed, DEFAULT_TIMEOUT)) {
                    actions.assertElementIsNotPresent(postTitle, NEW_POST_TITLE);
                }
            case "connections":
                //logic
                break;
            default:
                throw new IllegalStateException("Unexpected post type: " + type);
        }
    }

    public void createNewComment(){
        navigateToNewlyCreatedPost();
        actions.clickElement(leaveCommentField);
        actions.typeValueInField(NEW_COMMENT_DESCRIPTION, leaveCommentField);
        actions.clickElement(commentSaveButton);
    }

    public void assertCommentIsPosted(){
        navigateToNewlyCreatedPost();
        actions.assertElementPresent(commentTitle, NEW_COMMENT_DESCRIPTION);
    }

    public void deleteComment(){
        actions.clickElement(editCommentButton, NEW_COMMENT_DESCRIPTION);
        actions.clickElement(deleteCommentButton);
        actions.clickElement(commentSubmitButton);
    }

    public void assertCommentIsDeleted() {
        navigateToNewlyCreatedPost();
        if (actions.isElementPresentUntilTimeout(feed, DEFAULT_TIMEOUT)) {
            actions.assertElementIsNotPresent(commentTitle, NEW_COMMENT_DESCRIPTION);
        }
    }
}
