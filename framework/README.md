# UI Tests for Healthy Food Social Network

## Getting Started

The UI tests have been crafted using [IntelliJ IDEA](https://www.jetbrains.com/idea/) with the help of [Selenium WebDriver](https://www.selenium.dev) and [Maven](https://maven.apache.org). The automated tests are executed in [Mozilla Firefox](https://www.mozilla.org/en-US/firefox/new/) web browser.

### Pre-requisites

- **Healthy Food Social Network** to be hosted locally or on a server
- Java installed
- Maven installed
- Firefox installed

### Preparing the test environment
Make sure you can access **Healthy Food Social Network**. 

By default, the project is set for testing the application hosted locally on `http://localhost:8080` with username and password from the API script. You can change the settings by changing the following:

#### src/test/resources/config.properties
```
config.defaultTimeoutSeconds=4 //increase in case the server is slow
base.url={{application host (with / at the end)}}
...
healthyFoodAdmin.username={{administrator username)}}
healthyFoodAdmin.password={{administrator password)}}
regularUser.username={{regular user username)}}
regularUser.password={{regular user password)}}
```

## Running UI tests

For Windows users, navigate to the **framework** folder and double-click on **runTests.bat**, or alternatively, navigate to the folder using CMD or PowerShell, then type:
```
.\runTests.bat 
```

For macOS or Linux users, navigate to the **framework** folder using Terminal then type:
```
./runTests.sh
```

You will be presented the following menu:
```
Enter keys '1' to '5' to toggle test suites, 'x' to execute tests or 'q' to quit
Please select which test suites you would like to run:

[+] 1) Categories
[+] 2) Nationalities
[+] 3) Comments
[+] 4) Posts
[+] 5) Navigation

Enter your choice: █
```
Using the keys `1`, `2`, `3`, `4` and `5` you can toggle whether the test suites should be run or not. You should press enter after making a choice. In order to execute the test suites, press `x` and press enter. In order to quit without executing a test suite, press `q` and press enter.

After all the test suites finish running, you should have the following output:

```
...

Created the report in this location: /reports

Script finished executing successfully!
```

You'll find the report after the run in **framework/reports** folder as an .html file that you can preview using the browser.