# API Tests for Healthy Food Social Network

## Getting Started

The API tests have been crafted using [Postman](https://www.postman.com). The scripts use Node.js modules such as *newman* and *execsql* to execute the test suites and populate the database with test data.

### Pre-requisites

- **Healthy Food Social Network** to be hosted locally or on a server
- Node.js [(Download here)](https://nodejs.org/en/download/)

### Preparing the test environment
Make sure you can access **Healthy Food Social Network** as well as its database. 

By default, the project is set for testing the application hosted locally on `http://localhost:8080` with database hosted on `localhost` using XAMPP or WAMP with database named `testdb`. You can change the settings by changing the following:

#### db.config
```
host={{database host}}
username={{database username}}
password={{database password}}
```
#### environment.json

```
...
{
	"key": "baseUrl",
	"value": "{{application host (without / at the end)}}",
	"enabled": true
},
...
```

#### data.sql & cleanup.sql
```
...
use {{database name}};
...
```
## Installing dependencies

This is done only once, before the first run of the test script.

### Automatically

For Windows users, navigate to the **postman** folder and double-click on **install.bat**.

For macOS or Linux users, navigate to the **postman** folder using Terminal or alternative shell then type:
```
./install.sh
```

### Manually

Using CMD or PowerShell (for Windows users) or Terminal (for macOS and Linux users), type `npm i newman newman-reporter-htmlextra execsql -g`, then wait for the script to finish installing the dependencies globally. If you do not want to install them globally, navigate to the **postman** folder using the corresponding shell, then run the script without the `-g`.

## Running API tests

For Windows users, navigate to the **postman** folder and double-click on **runTests.bat**, or alternatively, navigate to the folder using CMD or PowerShell, then type:
```
.\runTests.bat 
```

For macOS or Linux users, navigate to the **postman** folder using Terminal then type:
```
./runTests.sh
```

You should see the script running, producing an output similar to the following:
```
Configuring database connection...
Executing database script...
Running API tests...
```
After which you will be presented the following menu:
```
Enter keys '1' to '6' to toggle collections, 'x' to execute tests or 'q' to quit
Please select which test suites you would like to run:

[+] 1) Categories
[+] 2) Comments
[+] 3) Connections
[+] 4) Nationalities
[+] 5) Posts
[+] 6) Users

Enter your choice: █
```
Using the keys `1`, `2`, `3`, `4`, `5` and `6` you can toggle whether the collections should be run or not. You should press enter after making a choice. In order to execute the test suites, press `x` and press enter. In order to quit without executing a test suite, press `q` and press enter.

After all the test suites finish running, you should have similar output:

```
Using htmlextra version 1.19.6
Newman Run Progress |████████████████████████████████████████| 100% || Requests: 49/49 || ETA: 0s

Created the htmlextra report in this location: /newman

Cleaning up posts...

Script finished executing successfully!
```

You'll find the report after the run in **postman/newman** folder as an .html file that you can preview using the browser.