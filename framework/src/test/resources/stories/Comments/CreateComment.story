Meta:
@endToEnd
@commentFunctionality

Narrative:
As an administrator
I want to comment a post
So that I can show comment functionality

Scenario: Comment a post
Given A post is created
When I'm posting a comment
Then The comment is posted