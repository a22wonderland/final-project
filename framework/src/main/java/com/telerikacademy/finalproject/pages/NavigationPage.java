package com.telerikacademy.finalproject.pages;

public class NavigationPage extends BasePage {
    public final String homeButton = "navigation.Home";
    public final String logOutButton = "navigation.LogOut";
    public final String signInButton = "navigation.SignIn";
    public final String signUpButton = "navigation.SignUp";
    public final String settingsMenu = "navigation.Settings";
    public final String categoriesButton = "navigation.Settings.Categories";
    public final String nationalitiesButton = "navigation.Settings.Nationalities";
    public final String adminPostsButton = "navigation.Settings.Posts";
    public final String postsMenu = "navigation.Posts";
    public final String newPostButton = "navigation.Posts.New";
    public final String connectedPostsButton = "navigation.Posts.Connected";
    public final String allPostsButton = "navigation.Posts.All";
    public final String myProfileMenu = "navigation.Profile";
    public final String myProfileButton = "navigation.Profile.Profile";
    public final String editProfileButton = "navigation.Profile.Edit";
    public final String usersMenu = "navigation.Users";
    public final String requestsButton = "navigation.Users.Requests";
    public final String friendsButton = "navigation.Users.Friends";
    public final String usersButton = "navigation.Users.Users";
    public final String aboutUsButton = "navigation.AboutUs";

    public void navigateTo(String page) {
        switch (page) {
            case "homepage":
                actions.clickElement(homeButton);
                break;
            case "login":
                actions.clickElement(signInButton);
                break;
            case "categories":
                actions.hoverMenuThenClickElement(settingsMenu, categoriesButton);
                break;
            case "nationalities":
                actions.hoverMenuThenClickElement(settingsMenu, nationalitiesButton);
                break;
            case "posts":
                actions.clickElement(postsMenu);
                break;
            case "all posts":
                actions.hoverMenuThenClickElement(postsMenu, allPostsButton);
                break;
            case "connected posts":
                actions.hoverMenuThenClickElement(postsMenu, connectedPostsButton);
                break;
            case "new post":
                actions.hoverMenuThenClickElement(postsMenu, newPostButton);
                break;
            case "profile":
                actions.clickElement(myProfileMenu);
                break;
            case "my profile":
                actions.hoverMenuThenClickElement(myProfileMenu, myProfileButton);
                break;
            case "edit profile":
                actions.hoverMenuThenClickElement(myProfileMenu, editProfileButton);
                break;
            case "users":
                actions.clickElement(usersMenu);
                break;
            case "requests":
                actions.hoverMenuThenClickElement(usersMenu, requestsButton);
                break;
            case "friends":
                actions.hoverMenuThenClickElement(usersMenu, friendsButton);
                break;
            case "all users":
                actions.hoverMenuThenClickElement(usersMenu, usersButton);
                break;
            case "about us":
                actions.clickElement(aboutUsButton);
                break;
            case "register":
                actions.clickElement(signUpButton);
                break;
        }
    }
}

