package stepdefinitions;

import com.telerikacademy.finalproject.pages.*;
import com.telerikacademy.finalproject.utils.UserActions;
import com.telerikacademy.finalproject.utils.Utils;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.File;

public class StepDefinitions extends BaseStepDefinitions{
    LoginPage loginPage = new LoginPage();
    PostsPage postsPage = new PostsPage();
    CategoryPage categoryPage = new CategoryPage();
    NationalityPage nationalityPage = new NationalityPage();
    NavigationPage navPage = new NavigationPage();

    // ############ Login ############

    @Given("I am not logged in")
    public void logOutAndReturnToHomepage(){
        loginPage.logOutAndReturnToHomepage();
    }

    @Given("I am logged in as $user")
    public void logInAs(String user){
        loginPage.logInAs(user);
    }

    // ############ Posts ############

    @When("I create a new $type post")
    public void createNewPost(String type){
        postsPage.createNewPost(type);
    }

    @When("I delete a $type post")
    public void deletePost(String type){
        postsPage.deletePost(type);
    }

    @Then("A new $type post is created")
    public void assertPostCreated(String type){
        postsPage.assertPostCreated(type);
    }

    @Then("A $type post is deleted")
    public void assertPostDeleted(String type){
        postsPage.assertPostDeleted(type);
    }

    // ########### Category ###########

    @When("I create a new category")
    public void createCategory(){
        categoryPage.createNewCategory();
    }

    @When("I delete a created category")
    public void deleteCategory(){
        categoryPage.deleteCategory();
    }

    @Then("A new category is created")
    public void assertCategoryIsCreated(){
        categoryPage.assertNewCategoryIsCreated();
    }

    @Then("The category is deleted")
    public void assertCategoryIsDeleted(){
        categoryPage.assertCategoryIsDeleted();
    }

    // ########## Nationality ##########

    @When("I create a new nationality")
    public void createNationality() {
        nationalityPage.createNewNationality();
    }

    @When("I delete a created nationality")
    public void deleteNationality() {
        nationalityPage.deleteNationality();
    }

    @Then("A new nationality is created")
    public void assertNationalityIsCreated() {
        nationalityPage.assertNewNationalityIsCreated();
    }

    @Then("The nationality is deleted")
    public void assertNationalityIsDeleted() {
        nationalityPage.assertNationalityIsDeleted();
    }

    // ########### Comment ###########

    @Given("A post is created")
    public void newPostWasCreated(){
        loginPage.logInAs("administrator");
        postsPage.createNewPost("for comments");
    }

    @Given ("A comment is posted")
    public void commentIsPosted(){
        loginPage.logInAs("administrator");
        postsPage.navigateToNewlyCreatedPost();
    }


    @When("I'm posting a comment")
    public void postComment(){
        postsPage.createNewComment();
    }

    @Then("The comment is posted")
    public void assertCommentIsPosted(){
        postsPage.assertCommentIsPosted();
    }

    @When ("I delete a post comment")
    public void deleteComment(){postsPage.deleteComment();}

    @Then ("The comment is deleted")
    public void assertCommentIsDeleted() {postsPage.assertCommentIsDeleted();}
}
